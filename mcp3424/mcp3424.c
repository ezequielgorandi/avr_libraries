/**
@defgroup gorandi_mcp3424 MCP3424 Library
@code #include"mcp3424.h" @endcode
@date: 14/01/2017 04:06:37 p. m.
@author: Ezequiel Gorandi
@version 1.0

@brief  
Librería desarrollada para el uso de los conversores A/D mcp3424.
 
@note
Descripción General: 
 	El mcp3424 utiliza protocolo I2C y genera un dato de 18bits.
  	El mismo puede medir en un rango entre -2.048V < x < 2.048.
  
Librerías requeridas:
 	- mcp3424.h
 	- twimaster.c
 	- i2cmaster.h
 
Modo de Uso:
	- Inicialización: Debe realizarse la inicialización del I2C.
 	
   	- Comienzo de conversión:
 		- Función: unsigned char mcp3424StartConversion ()
 		
 	- Lectura de Registro
 		- Función: unsigned char mcp3424ReadData(int32_t *valor)
**/
 

/**@{*/
#include "Main.h"


/**

 @brief Comienza la conversión del sensor conectado en la ADDR 0.
 		 //TODO: Permitir seleccionar el ADDR del que comenzará la conversión
 @param void
 @return unsigned char. Devolverá un código de error definido en mcp34724.h
 Notas
*/
unsigned char mcp3424StartConversion ()
{
	if ( i2c_start(MCP3424_ADDR0_WRITEMODE) )
		return MCP3424_ERROR ;
	if ( i2c_write(MCP3424_CH0_OS_18B_START) )
		return MCP3424_ERROR ;
	i2c_stop();
	return MCP3424_OK ;
}

/**
@brief Realiza la lectura de 18bits del sensor conectado en la ADDR 0.
 		   //TODO: Permitir seleccionar el ADDR del sensor
@param int32_t*. 
	   En esta variable se almacenará el valor medido en [uV].
		valor = sXXXXXX[uV]
 		siendo s el signo del valor medido 
@retval (unsigned char). Devolverá un código de error definido en mcp34724.h
*/
unsigned char mcp3424ReadData(int32_t *valor)
{
	char dataByte = 0 ;
	*valor = 0;
	
	//Configura al ADC en modo lectura
	if ( i2c_start(MCP3424_ADDR0_READMODE)==1 )
		return MCP3424_ERROR ;
	
	//Realiza la lectura de los 18 bits.
	dataByte = i2c_readAck();
	*valor = *valor + ((int32_t)dataByte<<(8*2));
	dataByte = i2c_readAck();
	*valor = *valor + ((int32_t)dataByte<<8);
	dataByte = i2c_readNak(); //La última lectura se realiza sin ACK
	*valor = *valor + ((int32_t)dataByte);

	
	if ( (*valor & ((int32_t)1<<17) ) )//Bit del signo
	{
		*valor = 0x1000000-*valor;
		*valor = *valor*(-1);	
	}
	*valor = ( (int64_t)(*valor)*2048*1000)/(131071+1);
	return MCP3424_OK;
}
/**@{*/
