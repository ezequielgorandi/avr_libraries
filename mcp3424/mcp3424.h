/**
@defgroup gorandi_mcp3424 MCP3424 Library
@date 15/01/2017 05:42:03 p. m.
@author Ezequiel Gorandi

@note 
MCP3424 Configuration:
	'mode' = MCP3424 8-bit configuration register
	see sec. 5.2 Configuration Register, p. 18 of datasheet

	@code
	b7  b6  b5  b4    b3  b2  b1  b0  ----- (bit number)
	RDY C1  C0  O/C   S1  S0  G1  G0  ----- (bit name)
	@endcode
	RDY = 0 when ready (or in one-shot, write '1' to start conversion)
	C1/C0: input channel # 00 = 0, 01 = 1, 10 = 2, 11 = 3
	O/C: 1 = continuous conversion mode, 0 =  single-shot conversion
	S1/S0: rate: 00 = 240 Hz (12 b), 01 = 60 Hz (14 b) 10 = 15 Hz (16 b) 11 = 3.75 Hz (18 b)
	G1/G0: gain: 00 = x1, 01 = x2, 10 = x4, 11 = x8    FullScale: 2.048 V, 1.024, 0.512, 0.256

MCP3424 configuration examples:
	@code
	0001 1100  0x1C is channel 0, continuous, 18-bit, gain x1
	0011 1100  0x3C is channel 1, continuous, 18-bit, gain x1
	1000 1100  0x8C is channel 0, One-Shot Conversion mode, 18-bit, gain x1
	@endcode

MCP3424 writing
	@code
	b7  b6  b5  b4    b3  b2  b1  b0  ----- (bit number)
	C3  C2  C1  C0    A2  A1  A0  R/W  ----- (bit name)
	@endcode
	C3-C0: Device Code (factory): 1101
	A2-A0: Address code. Configurable con los pines del integrado.
	R/W: 1-Read Mode, 0-Write mode
	Example:
	 1101 0000  0XD0, Address 0, write mode
*/
/**@{*/
#ifndef MCP3424_H_
#define MCP3424_H_
#endif /* MCP3424_H_ */

// --- Comandos de configuración ---
#define MCP3424_ADDR0_WRITEMODE 0xD0 ///< Configuración para un sensor configurado para el Address 0 modo escritura
#define MCP3424_ADDR0_READMODE 0xD1  ///< Configuración para un sensor configurado para el Address 0 modo lectura
#define MCP3424_CH0_OS_18B_START 0x8C

// --- Códigos de error ---
#define MCP3424_ERROR 0
#define MCP3424_OK 1 

unsigned char mcp3424StartConversion ();
unsigned char mcp3424ReadData(int32_t *);
/**@{*/
