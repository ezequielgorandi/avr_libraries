/*
 * rak811.c
 *
 * Created: 13/12/2017 05:19:56 p.m.
 *  Author: Ezequiel Gorandi
 */ 
// 
// 	if ( sendATcommand1("AT+CIFSR","ERROR",T0_10S) == 1 )
// 	{
// 		#ifdef _GPRS_DEBUG_
// 		uart_putsLb("AT+CIFSR Error");
// 		#endif // _GPRS_DEBUG_
// 		Recepcion_GPRS = AT_ERROR;
// 	}
// 	else
// 	{
// 		#ifdef _GPRS_DEBUG_
// 		uart_putsLb("AT+CIFSR OK: IP OPEN");
// 		#endif // _GPRS_DEBUG_
// 		Recepcion_GPRS = AT_OK ;
// 	}
#include <avr/io.h>
#include <string.h>
#include "rak811.h"
#include "../atCommand/atCommand.h"

RAK811_RESULT rak811_join()
{
	if (!sendATcommand1("at+mode=0","OK",5))
	{
		return RAK811_ERROR;
	}
	if (!sendATcommand1("at+set_config=app_eui:2222222222222222&app_key:22222222222222222222222222222222","OK",5))
	{
		return RAK811_ERROR;		
	}
	//	_delay_ms(5000);// ed agrega

	if (!sendATcommand1("at+join=otaa","3,0,0",180))
	{
		return RAK811_ERROR;
	}
	//	_delay_ms(5000);// ed agrega
	return RAK811_OK;
}

//El mensaje puede tener como maximo 64 bytes
RAK811_RESULT rak811_sendConfirmed(const char *message)
{
	char buf [80];
	strcpy(buf,"at+send=0,2,");
	strcat(buf, message);
	if (!sendATcommand1(buf,"2,0,0",180))
	return RAK811_ERROR;
	else
	return RAK811_OK;
}

/*
RAK811_RESULT rak811_join()
{
	if (!sendATcommand1("at+mode=0","OK",5))
	return RAK811_ERROR;
	if (!sendATcommand1("at+set_config=app_eui:39d7119f920f7952&app_key:a6b08140dae1d795ebfa5a6dee1f4dbd","OK",5))
	return RAK811_ERROR;
	if (!sendATcommand1("at+join=otaa","3,0,0",180))
	return RAK811_ERROR;
	return RAK811_OK;
}

//El mensaje puede tener como maximo 64 bytes
RAK811_RESULT rak811_sendConfirmed(const char *message)
{
	char buf [80];
	strcpy(buf,"at+send=0,2,");
	strcat(buf, message);
	if (!sendATcommand1(buf,"2,0,0",180))
	return RAK811_ERROR;
	else
	return RAK811_OK;
}
*/