/*
 * rak811.h
 *
 * Created: 13/12/2017 05:20:23 p.m.
 *  Author: Ezequiel
 */ 


#ifndef RAK811_H_
#define RAK811_H_


typedef enum
{
	RAK811_ERROR = 0,
	RAK811_OK
}RAK811_RESULT;

RAK811_RESULT rak811_join(void);
//El mensaje puede tener como maximo 64 bytes
RAK811_RESULT rak811_sendConfirmed(const char *message);


#endif /* RAK811_H_ */