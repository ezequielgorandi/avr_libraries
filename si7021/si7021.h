/**
@defgroup gorandi_si7021 SI7021 Library
@date 18/01/2017.
@author Ezequiel Gorandi
@version 1.0
*/ 
/**@{*/


//************************ SETUP ***************************

// --- Puertos de sensores ---
#define SI7021_2_PORT PORTL ///< Define la dirección de lectura de los puertos del sensores SI7021
#define SI7021_2_DDR DDRL ///< Definen la dirección los puertos donde se conectarán los sensores SI7021
#define SI7021_2_POWER PL7  ///< Definen el puerto ON/OFF del SI7021 N°1




#define SI7021_1_PORT PORTL ///< Define la dirección de lectura de los puertos del sensores SI7021
#define SI7021_1_DDR DDRL ///< Definen la dirección los puertos donde se conectarán los sensores SI7021
#define SI7021_1_POWER PL6  ///< Definen el puerto ON/OFF del SI7021 N°2

//< Enumeración de los sensores de humedad utilizados
enum 
{
	SI7021_1 = 0, ///< Sensor de humedad 1
	SI7021_2,	///< Sensor de humedad 2
	N_SI7021	///< Total de sensores a utilizar
};
//********************** FIN SETUP **************************

#define SI7021_ADDR_WRITE 0x80
#define SI7021_ADDR_READ 0x81

// --- I2C commands ---
#define RH_READ             0xE5
#define TEMP_READ           0xE3
#define POST_RH_TEMP_READ   0xE0
#define RESET               0xFE
#define USER1_READ          0xE7
#define USER1_WRITE         0xE6

// --- Códigos de error ---
#define SI7021_ERROR 0
#define SI7021_OK 1

uint8_t getHumidityWaitCRC (uint16_t * valor, uint8_t nSensor);
uint8_t getHumidityWait (uint16_t * valor, uint8_t nSensor);
uint8_t getTemperatureWait (uint16_t * valor, uint8_t nSensor);
uint8_t si7021otherOff(uint8_t);
uint8_t si7021on(uint8_t);
uint8_t si7021off(uint8_t);
void	si7021init();
/**@{*/