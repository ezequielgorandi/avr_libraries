/**
@defgroup gorandi_si7021 SI7021 Library
@code #include <si7021.h> @endcode
@date 12/08/2017.
@author Ezequiel Gorandi
@version 2.0
- Se incorporan funciones para la toma de temperatura

@brief
Librería desarrollada para el uso del sensor SI7021.

@note
Descripción General: 
	El SI7021 utiliza protocolo I2C. Este sensor no permite utilizar dos chips 
	de manera simultáneo sobre la línea, por lo que, en caso de necesitarse mas
	de uno, se deberá apagar el resto de los sensores.
 
Librerías requeridas:
	- si7021.h
	- twimaster.c
	- i2cmaster.h
	-delay.h
 
Modo de Uso:
	- Inicialización: Debe realizarse la inicialización del I2C.
		En caso de utilizarse
	
	- Comienzo de conversión:
		- Función: unsigned char mcp3424StartConversion ()
		
	- Lectura de Registro
		- Función: unsigned char mcp3424ReadData(int32_t *valor)
*/
/**@{*/

#include "Main.h"

/**
@brief	Apaga todos los sensores conectados.. 
		
@param	void
		 
@return void

@note  Enciende todos los sensores
*/
void si7021init()
{
	uint8_t i ;
	for ( i = 0 ; i<N_SI7021 ; i++)
	{
		si7021on( SI7021_1+i);
	}
}



/**
@brief	Inicia la conversión del sensor de humedad, espera a que se realice la 
		conversión y graba el valor medido.
		En caso de utilizar mas de un sensor, es necesario que solo uno se 
		encuentre conectado a la línea. Por esta razón esta función recibe el 
		sensor a leer para encenderlo durante la lectura y apagarlo una vez 
		finalizada. 
		Realiza el chksum del dato leido.
		
@param	uint16_t *. Variable donde se almacenará el valor medido.
 		*valor = 9500 --> RH% = 95.00%
		 
@param	uint8_t. En número de sensor que se desea leer.
		Este sensor debe estar definido en si7021.h y tener asignado
		un puerto.
		
@return unsigned char. SI7021_OK / SI7021_ERROR
*/
uint8_t getHumidityWaitCRC (uint16_t * valor, uint8_t nSensor)
{
	uint8_t crc = 0 ;
	uint8_t buffer[2];
	uint8_t flagError = SI7021_OK;

	if( si7021otherOff(nSensor) == SI7021_ERROR )
		flagError = SI7021_ERROR;
		
	//Envío el ADDR y lo seteo en modo Write
	if ( i2c_start(SI7021_ADDR_WRITE) == 1 )
	{
		si7021init();
		return SI7021_ERROR;//flagError = SI7021_ERROR;
	}
	//Envío el cmd para leer humedad
	if ( i2c_write(RH_READ) == 1 )
		flagError = SI7021_ERROR;
	
	//Lo configuro en modo READ
	if ( i2c_rep_start(SI7021_ADDR_READ) == 1 )
		flagError = SI7021_ERROR;

	_delay_ms(25); //Espero a que realice la conversión
	buffer[0] = i2c_readAck(); //Lectura de MSB
	buffer[1] = i2c_readAck(); //Lectura de LSB
	crc = i2c_readNak();
	
	//Chksum
	if ( crc8(buffer, crc) == TRUE )
		flagError = SI7021_ERROR;

	//Si hubo algún error, apaga el sensor y retorna error
	if ( flagError == SI7021_ERROR )
	{
		si7021init();
		*valor = 55555;
		return SI7021_ERROR;
	}
	
	i2c_stop(); //Termino la comunicación.
	
	si7021init(); //Enciendo los sensores

	*valor = buffer[1] + (buffer[0]<<8); //Llevo el valor al modo XXX.XX%
	*valor = (*valor * (uint32_t)12500)/65536-600 ;
	return SI7021_OK;
}


/**
@brief	Inicia la conversión del sensor temperatura, espera a que se realice la 
		conversión y graba el valor medido.
		En caso de utilizar mas de un sensor, es necesario que solo uno se 
		encuentre conectado a la línea. Por esta razón esta función recibe el 
		sensor a leer para encenderlo durante la lectura y apagarlo una vez 
		finalizada. 
		Realiza el chksum del dato leido.
		
@param	uint16_t *. Variable donde se almacenará el valor medido.
 		*valor = 24520 --> 24.5°C
		 
@param	uint8_t. En número de sensor que se desea leer.
		Este sensor debe estar definido en si7021.h y tener asignado
		un puerto.
		
@return unsigned char. SI7021_OK / SI7021_ERROR
*/
uint8_t getTemperatureWait (uint16_t * valor, uint8_t nSensor)
{
	uint8_t buffer[2];
	uint8_t flagError = SI7021_OK;

	if( si7021otherOff(nSensor) == SI7021_ERROR )
		flagError = SI7021_ERROR;
		
	//Envío el ADDR y lo seteo en modo Write
	if ( i2c_start(SI7021_ADDR_WRITE) == 1 )
	{
		si7021init();
		return SI7021_ERROR;//flagError = SI7021_ERROR;
	}
	//Envío el cmd para leer humedad
	if ( i2c_write(TEMP_READ) == 1 )
		flagError = SI7021_ERROR;
	
	//Lo configuro en modo READ
	if ( i2c_rep_start(SI7021_ADDR_READ) == 1 )
		flagError = SI7021_ERROR;

	_delay_ms(25); //Espero a que realice la conversión
	buffer[0] = i2c_readAck(); //Lectura de MSB
	buffer[1] = i2c_readNak(); //Lectura de LSB	
/*
	buffer[0] = i2c_readAck(); //Lectura de MSB
	buffer[1] = i2c_readAck(); //Lectura de LSB
	crc = i2c_readNak();
	
	//Chksum
	if ( crc8(buffer, crc) == TRUE )
		flagError = SI7021_ERROR;
*/
	//Si hubo algún error, apaga el sensor y retorna error
	if ( flagError == SI7021_ERROR )
	{
		si7021init();
		*valor = 55555;
		return SI7021_ERROR;
	}
	
	i2c_stop(); //Termino la comunicación.
	
	si7021init(); //Enciendo los sensores

	*valor = buffer[1] + (buffer[0]<<8); //Llevo el valor al modo XXX.XX%
	//Formula de la hoja de datos
	*valor = (*valor * (uint32_t)17572)/65536-4685 ;
	return SI7021_OK;
}

/**
@brief	Enciende el sensor seleccionado, llevando a 1 el puerto
		conectado a la alimentación del sensor.
		
@param	uint8_t. Sensor que se desea encender.

@return unsigned char. SI7021_OK / SI7021_ERROR
*/
uint8_t si7021on(uint8_t nSensor)
{
	switch (nSensor)
	{
		case SI7021_1:
		SI7021_1_DDR |= (1<<SI7021_1_POWER); //output
		SI7021_1_PORT &= ~(1<<SI7021_1_POWER); //low
		break;
		case SI7021_2:
		SI7021_2_DDR |= (1<<SI7021_2_POWER); //output
		SI7021_2_PORT &= ~(1<<SI7021_2_POWER); //low
		break;
		default:
		return SI7021_ERROR ;
		break;
	}
	return SI7021_OK ;
}
/**
@brief	Apaga el sensor seleccionado, llevando a 0 el puerto
		conectado a la alimentación del sensor.
		
@param	uint8_t. Sensor que se desea encender.

@return unsigned char. SI7021_OK / SI7021_ERROR
*/
uint8_t si7021off(uint8_t nSensor)
{
	switch (nSensor)
	{
		case SI7021_1:
		SI7021_1_DDR |= (1<<SI7021_1_POWER); //output
		SI7021_1_PORT &= ~ (1<<SI7021_1_POWER); //low
		break;
		case SI7021_2:
		SI7021_2_DDR |= (1<<SI7021_2_POWER); //output
		SI7021_2_PORT &= ~ (1<<SI7021_2_POWER); //low
		break;
		default:
		return SI7021_ERROR ;
		break;
	}
	return SI7021_OK ;
}
/**
@brief	Apaga el sensor que no va a utilizarse.
		
@param	uint8_t. Sensor que se desea encender.

@return unsigned char. SI7021_OK / SI7021_ERROR
*/
uint8_t si7021otherOff(uint8_t nSensor)
{
	switch (nSensor)
	{
		case SI7021_1:
			SI7021_2_DDR |= (1<<SI7021_2_POWER); //output
			SI7021_2_PORT |= (1<<SI7021_2_POWER); //low
		break;
		
		case SI7021_2:
			SI7021_1_DDR |= (1<<SI7021_1_POWER); //output
			SI7021_1_PORT |= (1<<SI7021_1_POWER); //low
		break;
		
		default:
		return SI7021_ERROR ;
		break;
	}
	return SI7021_OK ;
}

unsigned char si7021startConversion ()
{
	// if ( i2c_write(MCP3424_CH0_OS_18B_START) )
	// 	return SI7021_OK ;
	// i2c_stop();
	return SI7021_OK ;
}
/**@}*/

/**
@brief	Inicia la conversión del sensor de humedad, espera a que se realice la 
		conversión y graba el valor medido.
		En caso de utilizar mas de un sensor, es necesario que solo uno se 
		encuentre conectado a la línea. Por esta razón esta función recibe el 
		sensor a leer para encenderlo durante la lectura y apagarlo una vez 
		finalizada. 
		
@param	uint16_t *. Variable donde se almacenará el valor medido.
 		*valor = 9500 --> RH% = 95.00%
		 
@param	uint8_t. En número de sensor que se desea leer.
		Este sensor debe estar definido en si7021.h y tener asignado
		un puerto.
		
@return unsigned char. SI7021_OK / SI7021_ERROR
*/
/*
uint8_t getHumidityWait (uint16_t * valor, uint8_t nSensor)
{
	char buffer[2];
	uint8_t flagError = SI7021_OK;

 	if( si7021on(nSensor) == SI7021_ERROR ) 
 		flagError = SI7021_ERROR;
	
	//Envío el ADDR y lo seteo en modo Write
	if ( i2c_start(SI7021_ADDR_WRITE) == 1 ) 
		flagError = SI7021_ERROR;
		
	//Envío el cmd para leer humedad
	if ( i2c_write(RH_READ) == 1 ) 
		flagError = SI7021_ERROR;
		
	//Lo configuro en modo READ
	if ( i2c_rep_start(SI7021_ADDR_READ) == 1 ) 
		flagError = SI7021_ERROR;

	//Si hubo algún error, apaga el sensor y retorna error
	if ( flagError == SI7021_ERROR ) 
	{
		si7021off(nSensor); 
		*valor = 55555;
		return SI7021_ERROR;
	}
	
	_delay_ms(25); //Espero a que realice la conversión
	buffer[1] = i2c_readAck(); //Le ambos bytes de datos
	buffer[0] = i2c_readNak();
	//TODO: ADD CHECKSUM: Lee un byte mas
	i2c_stop(); //Termino la comunicación.
	
	si7021off(nSensor); //Apago el sensor

	*valor = buffer[0] + (buffer[1]<<8); //Llevo el valor al modo XXX.XX%
	*valor = (*valor * (uint32_t)12500)/65536-600 ;
	return SI7021_OK;
}
*/