/**
@defgroup gorandi_ds18s20 DS18S20 Library
@code 
	#include "ds18s20.h" 
	#include "delay.h"
@endcode
@date 15/01/2017.
@author Ezequiel Gorandi
@version 1.0

@brief
Librería desarrollada para el uso de los sensores ds18s20.

@note
-Descripción general: Este sensor utiliza un protocolo one wire. Una vez 
 recibida la instrucción, comienza la conversión A/D y almacena el dato en un 
 registro de 8 bits. Esta conversión puede durar hasta 750mS, por lo que es 
 conveniente haberla ya realizado al momento de realizar la lectura de datos.

-Librerías requeridas:
	- ds18s20.h
	- delay.h

-Configuraciones de ds18s20.h:
	- Setup de puertos (utiliza solo un puerto de entrada/salida).
	- Setup de cantidad de sensores

-Modo de Uso:
	- Inicialización:
	  	- Función: void ds18b20_init ()
	  	- Descripción: Encuentra a los sensores conectados. Carga las siguientes 
		  variables globales:
		  	unsigned char ds18s20Activos. Esta variable indica la cantidad de 
		  	sensores activos
  	- Comienzo de conversión:
		- Función: void ds18b20_startAllConversion()
		- Descripción: Todos los sensores conectados comienzan con la conversión
		  analógica digital
	- Medición de temperatura
		- Función: int ds18s20_gettemp(unsigned char nSensor) 
		- Descripción: Obtiene la temperatura del sensor indicado.
*/
/**@{*/
#include "Main.h"

//--- Variables Globales ---
unsigned char ROM[8]; ///< ROM Bit
unsigned char lastDiscrep = 0; ///< ds18s20 Last discrepancy
unsigned char doneFlag = 0; ///< ds18s20 Done flag
unsigned char FoundROM[11][8]; ///< ds18s20 Table of found ROM codes
volatile unsigned char numROMs; ///< ds18s20 rom selector
volatile unsigned char ds18s20Activos = 0 ; ///< Number of ds18s20 connected
unsigned char dowcrc; ///< ds18s20 crc

//! Tabla utilizada para el cálculo del CRC
unsigned char dscrc_table[] = {
	0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
	157,195, 33,127,252,162, 64, 30, 95, 1,227,189, 62, 96,130,220,
	35,125,159,193, 66, 28,254,160,225,191, 93, 3,128,222, 60, 98,
	190,224, 2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
	70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89, 7,
	219,133,103, 57,186,228, 6, 88, 25, 71,165,251,120, 38,196,154,
	101, 59,217,135, 4, 90,184,230,167,249, 27, 69,198,152,122, 36,
	248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91, 5,231,185,
	140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
	17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
	175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
	50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
	202,148,118, 40,171,245, 23, 73, 8, 86,180,234,105, 55,213,139,
	87, 9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
	233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53};





/**
@brief Todos los sensores conectados en la línea inician la conversión A/D.
@param void
@retval void
@note
 	Debe tenerse en cuenta que esta conversión puede demorar 750mS
*/
void ds18s20_startAllConversion ()
{
	ds18s20_reset();
	ds18s20_writebyte(DS18B20_CMD_SKIPROM); //skip ROM
	ds18s20_writebyte(DS18B20_CMD_CONVERTTEMP); //start temperature conversion
}

/**
@brief Resetea el sensor ds18s20.
@param void
@retval void
@note
 	- Debe ser utilizada antes de realizar la primer lectura. 
 	- Encuentra a los sensores conectados. Carga ds18s20Activos con la
 	  cantidad de sensores encontrados.
 	- Comienza la conversión A/D de todos los sensores.
*/
void ds18s20_init ()
{
	ds18s20_findDevices();
	ds18s20Activos = numROMs ;
	ds18s20_startAllConversion();
}

/**
@brief Lee el valor almacenado en el registro de 8 bits del sensor.
@param unsigned char nSensor. Esta variable indica el sensor que se desea leer.
@retval  int. Este entero será un valor signado de temperatura.
		   	  En caso de producirse un error devuelve 999 o 998, que son valores
		   	  de temperatura fuera del rango del sensor.
@note
 	- Debe tenerse en cuenta que al momento de leer el registro, previamente el
 	  sensor debe haber realizado la conversión A/D. Esta conversión puede durar
 	  hasta 750mS, por lo que es conveniente iniciarla inmediatamente después
	  de leer el registro.
*/
int ds18s20_gettemp(unsigned char nSensor) 
{
	char temp_msb = 0;
	char temp_lsb = 0;
	int temperatura ;
	char flagError = 1 ;

	#if DS18B20_STOPINTERRUPTONREAD == 1
	cli();
	#endif

		if(ds18s20_readbit()) //consulta si terminó la conversión	
		{
			if ( ds18s20_reset() == DS18S20_RESET_ERROR) //reset
			{
				flagError = 0 ;
			}
//			ds18b20_writebyte(DS18B20_CMD_SKIPROM); //skip ROM
			ds18s20_writebyte(DS18B20_CMD_MATCHROM); //Match ROM
			//Utiliza la variable global para seleccionar el sensor. TODO: Podría modificarse para enviar como parámetro
			numROMs = nSensor ; 
			if (ds18s20_sendMatchRom()) //Si es false hay error
			{
				ds18s20_writebyte(DS18B20_CMD_RSCRATCHPAD); //read scratchpad
				temp_lsb=ds18s20_readbyte(); //Byte menos significativo: Valor Absoluto de temperatura
				temp_msb=ds18s20_readbyte(); //Byte mas significativo: Signo de la temperatura
			}
			else
			{
				flagError = 0;
			}

			if (temp_msb == 0)
			{
				temperatura = (temp_lsb*10)/2; // Trabajo con enteros. Si temperatura = 275 => 27.5°
			}
			else
			{
				temperatura = (temp_lsb-256)*10/2;
			}
		}
		else
		{
			flagError = 0 ;
		}
	#if DS18B20_STOPINTERRUPTONREAD == 1
	sei();
	#endif
	if (flagError)
		return temperatura;
	else
		return DS18S20_ERROR;
}

/**
@brief Lee el valor almacenado en el registro de 8 bits del sensor. 
	   Realiza la comprobación del checksum. El sensor transmite 9 bytes. El
	   cálculo se realiza con los primeros 8 bytes y se lo compara con el noveno
	   que es el crc. 
@param unsigned char nSensor. Esta variable indica el sensor que se desea leer.
@retval  int. Este entero será un valor signado de temperatura.
		   	  En caso de producirse un error devuelve 999 o 998, que son valores
		   	  de temperatura fuera del rango del sensor.
@note
 	- Debe tenerse en cuenta que al momento de leer el registro, previamente el
 	  sensor debe haber realizado la conversión A/D. Esta conversión puede durar
 	  hasta 750mS, por lo que es conveniente iniciarla inmediatamente después
	  de leer el registro.
*/
int ds18s20_gettempCRC(unsigned char nSensor)
{
	int temperatura ;
	char flagError = 1 ;
	uint8_t data [9];
	uint8_t i = 0 ;
	uint8_t crc = 0 ;

	#if DS18B20_STOPINTERRUPTONREAD == 1
	cli();
	#endif

	if(ds18s20_readbit()) //consulta si terminó la conversión
	{
		if ( ds18s20_reset() == DS18S20_RESET_ERROR) //reset
		{
			flagError = 0 ;
		}
		//			ds18b20_writebyte(DS18B20_CMD_SKIPROM); //skip ROM
		ds18s20_writebyte(DS18B20_CMD_MATCHROM); //Match ROM
		//Utiliza la variable global para seleccionar el sensor. TODO: Podría modificarse para enviar como parámetro
		numROMs = nSensor ;
		if (ds18s20_sendMatchRom()) //Si es false hay error
		{
			ds18s20_writebyte(DS18B20_CMD_RSCRATCHPAD); //read scratchpad
			for (i = 0 ; i<9 ; i++)
			{
				data[i]=ds18s20_readbyte();
			}	
			for (i = 0 ; i<8 ; i++)
			{	
				crc = _crc_ibutton_update(crc, data[i]);		
			}
			if ( crc != data[8] ) //Si el chksum no coincide, hay error
				flagError = 0 ;
		}
		else
		{
			flagError = 0;
		}

		if (data[1] == 0)
		{
			temperatura = (data[0]*10)/2; // Trabajo con enteros. Si temperatura = 275 => 27.5°
		}
		else
		{
			temperatura = (data[0]-256)*10/2;
		}
	}
	else
	{
		flagError = 0 ;
	}
	#if DS18B20_STOPINTERRUPTONREAD == 1
		sei();
	#endif
	if (flagError)
		return temperatura;
	else
		return DS18S20_ERROR;
}



/**
@brief Resetea el sensor ds18s20.
@param void
@reval uint8_t. DS18S20_RESET_ERROR / DS18S20_RESET_OK
 */
uint8_t ds18s20_reset() //TODO: No reconoce la falta de respuesta al reset
{
	uint8_t i;

	//low for 480us
	DS18S20_PORT &= ~ (1<<DS18S20_DQ); //low
	DS18S20_DDR |= (1<<DS18S20_DQ); //output
	_delay_us(480);

	//release line and wait for 60uS
	DS18S20_DDR &= ~(1<<DS18S20_DQ); //input
	//_delay_us(60);
	_delay_us(60);

	//get value and wait 420us
	i = (DS18S20_PIN & (1<<DS18S20_DQ));
	_delay_us(420);
	
	if(i) //read value, 0=ok, 1=error
	{
		return DS18S20_RESET_ERROR;
	}
	else
	{
		return DS18S20_RESET_OK;
	}

}

/**
 * write one bit
 */
void ds18b20_writebit(uint8_t bit)
{
	//low for 2uS
	DS18S20_PORT &= ~ (1<<DS18S20_DQ); //low
	DS18S20_DDR |= (1<<DS18S20_DQ); //output
	_delay_us(2);

	//if we want to write 1, release the line (if not will keep low)
	if(bit)
		DS18S20_DDR &= ~(1<<DS18S20_DQ); //input

	//wait 60uS and release the line
	_delay_us(70);
	DS18S20_DDR &= ~(1<<DS18S20_DQ); //input
	//_delay_us(2); //recuperacion
}

/*
 * read one bit
 */
uint8_t ds18s20_readbit(){
	uint8_t bit=0;

	//low for 1uS
	DS18S20_PORT &= ~ (1<<DS18S20_DQ); //low
	DS18S20_DDR |= (1<<DS18S20_DQ); //output
	_delay_us(2);

	//release line and wait for 14uS
	DS18S20_DDR &= ~(1<<DS18S20_DQ); //input
	_delay_us(10);

	//read the value
	if(DS18S20_PIN & (1<<DS18S20_DQ))
		bit=1;

	//wait 45uS and return read value
	_delay_us(50);
	return bit;
}

/*
 * write one byte
 */
void ds18s20_writebyte(uint8_t byte){
	uint8_t i=8;
	while(i--){
		ds18b20_writebit(byte&1);
		byte >>= 1;
		//_delay_us(10);
	}
}

/*
 * read one byte
 */
uint8_t ds18s20_readbyte(){
	uint8_t i=8, n=0;
	while(i--){
		n >>= 1;
		n |= (ds18s20_readbit()<<7);
	}
	return n;
}


/*
 * perform Match Rom
 */
unsigned char ds18s20_sendMatchRom()
{
	unsigned char i;
	if(ds18s20_reset()) return FALSE;
	ds18s20_writebyte(0x55); // match ROM
	for(i=0;i<8;i++)
	{
		ds18s20_writebyte(FoundROM[numROMs][i]); //send ROM code
	}
	return TRUE;
}

/*
 * Find devices
 */
void ds18s20_findDevices()
{
	unsigned char m;
	if(!ds18s20_reset()) //Begins when a presence is detected
	{
		if(ds18s20_first()) //Begins when at least one part is found
		{
			numROMs=0;
			do
			{
				for(m=0;m<8;m++) //Cantidad de Bytes
				{			
					FoundROM[numROMs][m]=ROM[m]; //Identifies ROM number on found device
				}
				numROMs++; 
			 //Continues until no additional devices are found
			} while (ds18s20_next()&&(numROMs<N_DS18S20)); 
		}
	}
}


/*
* The First function resets the current state of a ROM search and calls
* Next to find the first device on the 1-Wire bus.
*/
unsigned char ds18s20_first()
{
	lastDiscrep = 0; // reset the rom search last discrepancy global
	doneFlag = FALSE;
	return ds18s20_next(); // call Next and return its return value
}

/* NEXT
* The Next function searches for the next device on the 1-Wire bus. If
* there are no more devices on the 1-Wire then false is returned.
*/
unsigned char ds18s20_next()
{
	unsigned char m = 1; // ROM Bit index
	unsigned char n = 0; // ROM Byte index
	unsigned char k = 1; // bit mask
	unsigned char x = 0;
	unsigned char discrepMarker = 0; // discrepancy marker
	unsigned char g; // Output bit
	unsigned char nxt; // return value
	int flag;
	nxt = FALSE; // set the next flag to false
	dowcrc = 0; // reset the dowcrc
	flag = ds18s20_reset(); // reset the 1-Wire
	if(flag||doneFlag) // no parts -> return false
	{
		lastDiscrep = 0; // reset the search
		return FALSE;
	}
	ds18s20_writebyte(0xF0); // send SearchROM command
	do
	// for all eight bytes
	{
		x = 0;
		if(ds18s20_readbit()==1) 
			x = 2;
		_delay_us(120);
		if(ds18s20_readbit()==1) 
			x |= 1; // and its complement
		if(x ==3) // there are no devices on the 1-Wire
			break;
		else
		{
			if(x>0) // all devices coupled have 0 or 1
				g = x>>1; // bit write value for search
			else
			{
				// if this discrepancy is before the last
				// discrepancy on a previous Next then pick
				// the same as last time
				if(m<lastDiscrep)
					g = ((ROM[n]&k)>0);
				else // if equal to last pick 1
					g = (m==lastDiscrep); // if not then pick 0
				// if 0 was picked then record
				// position with mask k
				if (g==0) discrepMarker = m;
			}
			if(g==1) // isolate bit in ROM[n] with mask k
				ROM[n] |= k;
			else
				ROM[n] &= ~k;
			ds18b20_writebit(g); // ROM search write
			m++; // increment bit counter m
			k = k<<1; // and shift the bit mask k
			if(k==0) // if the mask is 0 then go to new ROM
			{ // byte n and reset mask
				ds18s20_owCrc(ROM[n]); // accumulate the CRC
				n++; k++;
			}
		}
	} while(n<8); //loop until through all ROM bytes 0-7
		
		if(m<65||dowcrc) {// if search was unsuccessful then
			lastDiscrep=0; // reset the last discrepancy to 0
		}
		else
		{
			// search was successful, so set lastDiscrep,
			// lastOne, nxt
			lastDiscrep = discrepMarker;
			doneFlag = (lastDiscrep==0);
			nxt = TRUE; // indicates search is not complete yet, more
			// parts remain
		}
		return nxt;
	}


/* 
* ONE WIRE CRC
*/
unsigned char ds18s20_owCrc( unsigned char x)
{
	dowcrc = dscrc_table[dowcrc^x];
	return dowcrc;
}
/**@{*/