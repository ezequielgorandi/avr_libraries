/**
@defgroup gorandi_ds18s20 DS18S20 Library
@date 15/01/2017
@author Ezequiel Gorandi
@version 1.0
*/
/**@{*/
#ifndef DS18B20_H_
#define DS18B20_H_


//************************ SETUP ***************************
//--- setup de puertos ---
// - Debe indicarse el puerto al cual estará conectado la línea de datos del
//   sensor
#define DS18S20_PORT PORTA ///< Define la dirección de escritura de los puertos utilizados por el sensor DS18S20
#define DS18S20_DDR DDRA ///< Define la dirección los puertos donde se conectarán los sensores SI7021
#define DS18S20_PIN PINA ///< Define la dirección de lectura de los puertos utilizados por el sensor DS18S20
#define DS18S20_DQ PA1  ///< Definen el puerto que utilizarán los sensores DS18S20 como línea de datos.

// --- Cantidad de sensores ---
#define  N_DS18S20 3 ///< Debe indicarse la cantidad de sensores DS18S20 utilizar.

//********************** FIN SETUP **************************

//******************* Codigos de error **********************
#define DS18S20_ERROR 999
#define DS18S20_DESCONECTADO 998
#define DS18S20_RESET_ERROR 1
#define DS18S20_RESET_OK 0

//******************* Comandos de I2C **********************
// Comandos utilizados por las funciones, no deben ser empleados por el usuario.
#define DS18B20_CMD_CONVERTTEMP 0x44
#define DS18B20_CMD_RSCRATCHPAD 0xbe
#define DS18B20_CMD_WSCRATCHPAD 0x4e
#define DS18B20_CMD_CPYSCRATCHPAD 0x48
#define DS18B20_CMD_RECEEPROM 0xb8
#define DS18B20_CMD_RPWRSUPPLY 0xb4
#define DS18B20_CMD_SEARCHROM 0xf0
#define DS18B20_CMD_READROM 0x33
#define DS18B20_CMD_MATCHROM 0x55
#define DS18B20_CMD_SKIPROM 0xcc
#define DS18B20_CMD_ALARMSEARCH 0xec

//stop any interrupt on read
#define DS18B20_STOPINTERRUPTONREAD 1

//**************** Declaración de funciones *******************
int ds18s20_gettempCRC(unsigned char);
extern int ds18s20_gettemp(unsigned char);
extern void ds18s20_init ();
unsigned char ds18s20_first();
void ds18s20_findDevices();
void ds18s20_startAllConversion ();
unsigned char ds18s20_next();
unsigned char ds18s20_owCrc( unsigned char );
unsigned char ds18s20_sendMatchRom();
uint8_t ds18s20_reset();
void ds18s20_writebyte(uint8_t);
uint8_t ds18s20_readbyte();
uint8_t ds18s20_readbit();

#endif
/**@{*/