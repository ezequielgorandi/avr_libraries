#README

## Descripción ##
Permite manejar comandos AT, indicando el mensaje a transmitir, el mensaje 
esperado y el timeout.

###Proyectos que la utilizan

* Datasender. Edgardo Mora (Probado)
* Prüfstand, Master. Edgardo Mora (Probado)
###Librerías necesarias

* string.h
* delay.h
* timer1.h
* uart.h"

## Versiones ##

### Version 1.00 ###
* Fecha  13/12/2017


### Who do I talk to? ###
* Ezequiel Gorandi ezequielgorandi@gmail.com