/*
 * atCommand.h
 *
 * Created: 13/12/2017 05:32:31 p.m.
 *  Author: Ezequiel
 */ 


#ifndef ATCOMMAND_H_
#define ATCOMMAND_H_

/**
 * \brief 
 * This function sends an ATcommand and checks if the answer is the expected
 * one.
 * \param ATcommand. It should be "at+aCommand"
 * \param expected_answer1. For example: "ok"
 * \param timeout [sec]
 * 
 * \return int8_t. 1 if the answer is the expected. 0 if not.
 */
int8_t sendATcommand1( char* ATcommand,  char* expected_answer1, unsigned int timeout);
int8_t sendATcommand2( char* ATcommand,  char* expected_answer1, char* expected_answer2, unsigned int timeout);




#endif /* ATCOMMAND_H_ */