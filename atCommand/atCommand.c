/*
 * atCommand.c
 *
 * Created: 13/12/2017 05:32:15 p.m.
 * Author: Ezequiel Gorandi
 * email: ezequielgorandi@gmail.com 
 */ 
#include <avr/io.h>
#include <string.h>
#include <util/delay.h>
#include "../timer1.h"
#include "../uart.h"
//#define UART_PUTSLB uart1_putsLb
//#define UART_NEWDATA uart1_newData

#define _AT_DEBUG_ 1

/**
 * \brief 
 * This function sends an ATcommand and checks if the answer is the expected
 * one.
 * \param ATcommand. It should be "at+aCommand"
 * \param expected_answer1. For example: "ok"
 * \param timeout [sec]
 * 
 * \return int8_t. 1 if the answer is the expected. 0 if not.
 */
int8_t sendATcommand1( char* ATcommand,  char* expected_answer1,
unsigned int timeout)
{
	uint8_t x=0,  answer=0;
	char response[100]; //TODO: MODIFICAR PARA QUE EL BUFFER SEA MAS CHICO

	memset(response,0, 100);    // Llena el buffer con ''

	//uart1_flush();    // Clean the input buffer

	uart1_putsLb(ATcommand);    // Send the AT command

	
	#if _AT_DEBUG_ == 1
		uart_puts("\n\rEnviado: ");
		uart_puts(ATcommand);
	#endif
	x = 0;
	// this loop waits for the answer
//TODO:DESCOMENTAR Y AgrEGAR	
	setTimeOut_S_Timer1(AT_CMD_TIMEOUT,timeout);
	do{
		// if there are data in the UART input buffer, reads it and checks for the asnwer
		if(uart1_newData())
		{
			response[x] = uart1_getc();
			x++;
			// check if the desired answer 1  is in the response of the module
			if (strstr(response, expected_answer1) != NULL)
			{
				answer = 1;
			}
		}
	}
	// Waits for the asnwer with time out
	while((answer == 0) && !timeOut_Timer1(AT_CMD_TIMEOUT));

	#if _AT_DEBUG_
		response[x] = '\0';
		uart_puts("\n\rRecibido: ");
		uart_puts(response);
	#endif

	return answer;
}


int8_t sendATcommand2( char* ATcommand,  char* expected_answer1,
char* expected_answer2, unsigned int timeout)
{
	uint8_t x=0,  answer=0;
	char response[100];

	memset(response,0, 100);    // Llena el buffer con ''

	//uart1_flush();    // Clean the input buffer

	uart1_putsLb(ATcommand);    // Send the AT command

	x = 0;
	// this loop waits for the answer
	setTimeOut_S_Timer1(AT_CMD_TIMEOUT,5);
	do{
		// if there are data in the UART input buffer, reads it and checks for the asnwer
		if(uart1_newData())
		{
			response[x] = uart1_getc();
			x++;
			// check if the desired answer 1  is in the response of the module
			if (strstr(response, expected_answer1) != NULL)
			{
				answer = 1;
			}
			// check if the desired answer 2 is in the response of the module
			else if (strstr(response, expected_answer2) != NULL)
			{
				answer = 2;
			}
		}
	}
	// Waits for the asnwer with time out
	while((answer == 0) && !timeOut_Timer1(AT_CMD_TIMEOUT));
	return answer;
}
