/*
 * cayenne.c
 *
 * Created: 30/12/2017 19:02:02
 * Author: Ezeqiuel Gorandi
 * e-mai:ezequielgorandi@gmail.com
 */ 

#include <avr/io.h>
#include <string.h>
#include "cayenne.h"
#include "../asciiTools.h"

uint8_t addTemperature(uint8_t channel, float celsius, uint8_t *buffer, uint8_t cursor)
{
	if ( (cursor + LPP_TEMPERATURE_SIZE) > IPSO_BUFFER_SIZE) {
		return 0;
	}
	int16_t val = celsius * 10;
	itoa_hex_2d(channel,buffer+cursor);
	cursor++;
	cursor++;
	itoa_hex_2d(LPP_TEMPERATURE,buffer+cursor);
	cursor++;
	cursor++;
	itoa_hex_2d((uint8_t)(val >> 8),buffer+cursor);
	cursor++;
	cursor++;
	itoa_hex_2d((uint8_t)(val),buffer+cursor);
	cursor++;
	cursor++;
	buffer[cursor]='\0';
	return cursor;
}

//0168
uint8_t addRelativeHumidity (uint8_t channel, float rh, uint8_t *buffer, uint8_t cursor)
{
	if ((cursor + LPP_RELATIVE_HUMIDITY_SIZE) > IPSO_BUFFER_SIZE) {
		return 0;
	}
	itoa_hex_2d(channel,buffer+cursor);
	cursor++;
	cursor++;
	itoa_hex_2d(LPP_RELATIVE_HUMIDITY,buffer+cursor);
	cursor++;
	cursor++;
	itoa_hex_2d((uint8_t)(rh * 2),buffer+cursor);
	cursor++;
	cursor++;
	buffer[cursor]='\0';
	return cursor;
}

uint8_t addAnalogInput(uint8_t channel, float value, uint8_t *buffer, uint8_t cursor)
{
	if ((cursor + LPP_ANALOG_INPUT) > IPSO_BUFFER_SIZE) {
		return 0;
	}
	int16_t val = value * 100;
	itoa_hex_2d(channel,buffer+cursor);
	cursor++;
	cursor++;
	itoa_hex_2d(LPP_ANALOG_INPUT,buffer+cursor);
	cursor++;
	cursor++;
	itoa_hex_2d((uint8_t)(val >> 8),buffer+cursor);
	cursor++;
	cursor++;
	itoa_hex_2d((uint8_t)(val),buffer+cursor);
	cursor++;
	cursor++;
	buffer[cursor]='\0';
	return cursor;
}
